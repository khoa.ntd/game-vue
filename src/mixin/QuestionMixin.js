import axios from "axios";

export const QuestionMixin = {
    data() {
        return {
            i: 0,
            questions: Array,
            loading: true,
            money: 0
        }
      },
      methods: {
        getQuestions() {
            this.loading = true;
            axios.get('http://game-vue.herokuapp.com/api/questions')
            .then((response) => {
              this.questions = response.data
            }).catch((error) => {
              console.log(error);
            }).finally(() => (this.loading = false))
          },
      },
      created() {
        this.getQuestions()
      }
}