import { createStore } from 'vuex'
import Question from './modules/Question'
import Final from './modules/Final'

const store = createStore({
    modules: {
        Question,
        Final
    }
})

export default store