import axios from 'axios'

export default {
    state: {
        questions: Array,
        loading: false,
        i: 0,
        money: 0,
        complete:false
    },
    mutations: {
        SET_QUESTIONS: (state, questions) => {
            state.questions = questions;
        },
        SET_LOADING: (state, boolean) => {
            state.loading = boolean
        },
        SET_I: (state, number) => {
            state.i += number
        },
        SET_MONEY: (state, number) => {
            state.money += number
        },
        SET_COMPLETE: (state, boolean) => {
            state.complete = boolean
        },
        SET_BEGIN_I: (state, number) => {
            state.i = number
        },
        SET_MONEY_BEGIN: (state, number) => {
            state.money = number
        }
    },
    actions: {
        SET_QUESTIONS: ({ commit }) => {
            commit('SET_LOADING', true);
            axios.get('https://game-vue.herokuapp.com/api/questions')
            .then((response) => {
              commit('SET_QUESTIONS', response.data)
            }).catch((error) => {
              console.log(error);
            }).finally(() => (commit('SET_LOADING', false)))
        }
    }
}