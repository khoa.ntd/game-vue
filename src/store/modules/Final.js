/* eslint-disable */
import axios from "axios";

export default {
  state: {
    name: String,
    point: 0,
    players: Array,
    loading_player: false,
  },
  mutations: {
    SET_NAME: (state, name) => {
      state.name = name;
    },
    SET_POINT: (state, point) => {
      state.point = point;
    },
    SET_PLAYER: (state, players) => {
      state.players = players;
    },
    SET_LOADING_PLAYER: (state, boolean) => {
        state.loading_player = boolean;
    }
  },
  actions: {
    ADD_POINT_TO_SERVER: ({ commit }, final) => {
      console.log(final);
      axios
        .post("https://game-vue.herokuapp.com/api/store-player", final)
        .then((response) => {
          alert(response.data);
        })
        .catch((error) => {
          alert(error);
        });
    },
    SET_PLAYERS: ({ commit }) => {
        commit('SET_LOADING_PLAYER', true);
        axios.get('https://game-vue.herokuapp.com/api/get-player')
        .then((response) => {
            commit('SET_PLAYER', response.data)
        }).catch((error) => {
            console.log(error);
        }).finally(() => (commit('SET_LOADING_PLAYER', false)))
    },
  },
  getter: {},
};
